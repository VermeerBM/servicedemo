# README #

Dit is de demo code die gebruikt is bij de Blue4IT kennissessie van 25 Jan 2018
Het laat zien hoe simpel een (micro) rest-service op te bouwen mbv Spring boot en MongoDB en hoe deze te testen is mbv Rest-Assured en WireMock

### Application ###

* Spring Boot RestService met TestCode in Rest-Assured en WireMock
* 0.0.1

### How do I get set up? ###

* Install mongoDB (gebruik bijv brew)
* Install Java 8
* Install maven
* Run Spring Boot application

### Who do I talk to? ###

* Brian Vermeer
* Other community or team contact