package brianvermeer.blue4it.springboot.servicedemo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import brianvermeer.blue4it.springboot.servicedemo.domain.Beer;

public interface BeerRepository extends MongoRepository<Beer, String>{

    Optional<Beer> findByName(String name);
    List<Beer> findByAlcoholGreaterThanEqual(double Beer);

}
