package brianvermeer.blue4it.springboot.servicedemo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BeerWithScore {

    private Beer beer;
    private int score;
}
