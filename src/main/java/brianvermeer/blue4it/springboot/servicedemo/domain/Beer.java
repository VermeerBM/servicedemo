package brianvermeer.blue4it.springboot.servicedemo.domain;


import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Beer {

    @Id
    private final String name;
    private final double alcohol;
}
