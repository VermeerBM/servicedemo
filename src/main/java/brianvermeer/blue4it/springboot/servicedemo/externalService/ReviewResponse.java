package brianvermeer.blue4it.springboot.servicedemo.externalService;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ReviewResponse {

    private String name;
    private int score;
}
