package brianvermeer.blue4it.springboot.servicedemo.externalService;

import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/review")
public class ReviewController {

    public static final int MIN = 0;
    public static final int MAX = 10;

    @GetMapping(path="/{name}")
    public ReviewResponse getScore(@PathVariable String name) {
        Random random = new Random();
        int score = random.nextInt(MAX - MIN + 1) + MIN;
        return new ReviewResponse(name, score);
    }


}
