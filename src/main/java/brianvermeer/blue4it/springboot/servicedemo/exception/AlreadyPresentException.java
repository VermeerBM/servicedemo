package brianvermeer.blue4it.springboot.servicedemo.exception;

public class AlreadyPresentException extends RuntimeException {
    public AlreadyPresentException(String s) {
        super(s);
    }
}
