package brianvermeer.blue4it.springboot.servicedemo.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException(String s) {
        super(s);
    }
}
