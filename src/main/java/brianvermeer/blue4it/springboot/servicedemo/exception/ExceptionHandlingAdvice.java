package brianvermeer.blue4it.springboot.servicedemo.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice("brianvermeer.blue4it.springboot.servicedemo")
public class ExceptionHandlingAdvice {

    private static final String MESSAGE_KEY = "message";


    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity handleNotFound(NotFoundException exception) {
        log.warn("A resource was not found. {}", exception.getMessage());
        return responseEntity(exception.getMessage(), NOT_FOUND);
    }

    @ExceptionHandler(AlreadyPresentException.class)
    public ResponseEntity handleAlreadyPresent(AlreadyPresentException exception) {
        log.warn("A bad request was received. {}", exception.getMessage());
        return responseEntity(exception.getMessage(), BAD_REQUEST);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity clientException(HttpClientErrorException exception) {
        log.warn("Something when wrong {}", exception.getMessage());
        return responseEntity("Something went wrong internally", INTERNAL_SERVER_ERROR);
    }


    private ResponseEntity<Map<String, String>> responseEntity(String value, HttpStatus status) {
        Map<String, String> responseMap = new HashMap<>();
        responseMap.put(MESSAGE_KEY, value);
        return new ResponseEntity<>(responseMap, status);
    }
}
