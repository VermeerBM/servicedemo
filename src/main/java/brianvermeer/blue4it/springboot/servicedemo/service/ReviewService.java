package brianvermeer.blue4it.springboot.servicedemo.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import brianvermeer.blue4it.springboot.servicedemo.domain.Beer;
import brianvermeer.blue4it.springboot.servicedemo.externalService.ReviewResponse;

@Service
public class ReviewService {

    private final String url;

    public ReviewService(@Value("${service.review.url}") String url) {
        this.url = url;
    }

    public ReviewResponse getReviewForBeer(Beer beer) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url + "/" + beer.getName());

        RestTemplate template = new RestTemplate();
        return template.getForObject(builder.build().encode().toUri(), ReviewResponse.class);
    }

}
