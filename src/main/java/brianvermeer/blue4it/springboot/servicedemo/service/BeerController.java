package brianvermeer.blue4it.springboot.servicedemo.service;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import brianvermeer.blue4it.springboot.servicedemo.domain.Beer;
import brianvermeer.blue4it.springboot.servicedemo.domain.BeerWithScore;
import brianvermeer.blue4it.springboot.servicedemo.exception.AlreadyPresentException;
import brianvermeer.blue4it.springboot.servicedemo.exception.NotFoundException;
import brianvermeer.blue4it.springboot.servicedemo.externalService.ReviewResponse;
import brianvermeer.blue4it.springboot.servicedemo.repository.BeerRepository;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/beers")
public class BeerController {

    private BeerRepository repository;
    private ReviewService reviewService;

    @GetMapping()
    public List<Beer> findAll() {
        return repository.findAll();
    }

    @GetMapping(path = "/{name}")
    public Beer findBeer(@PathVariable String name) {
        return repository.findByName(name).orElseThrow(() -> new NotFoundException("No beer found for name: " + name));
    }

    @PostMapping(path = "/add")
    public void addBeer(@RequestBody Beer beer) {
        repository.findByName(beer.getName())
            .ifPresent(beer_ -> { throw new AlreadyPresentException("Already have a beer with name: " + beer_.getName());});
        repository.save(beer);
    }

    @DeleteMapping(path = "/{name}")
    public void removeBeer(@PathVariable String name) {
        repository.findByName(name)
            .ifPresent(beer -> repository.delete(beer));
    }

    @GetMapping(path = "/{name}/withscore")
    public BeerWithScore getBeerWithScore(@PathVariable String name) {
        Beer beer = findBeer(name);
        ReviewResponse review = reviewService.getReviewForBeer(beer);
        return new BeerWithScore(beer, review.getScore());
    }


}
