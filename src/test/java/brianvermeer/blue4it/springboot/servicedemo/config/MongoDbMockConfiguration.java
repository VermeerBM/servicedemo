package brianvermeer.blue4it.springboot.servicedemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;

@Configuration
@Profile("test")
public class MongoDbMockConfiguration extends AbstractMongoConfiguration {

    @Override
    protected String getDatabaseName() {
        return "testing-db";
    }

    @Bean
    @Override
    public Mongo mongo() {
        return new Fongo(getDatabaseName()).getMongo();
    }
}
