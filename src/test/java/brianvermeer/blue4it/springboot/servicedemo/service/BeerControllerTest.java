package brianvermeer.blue4it.springboot.servicedemo.service;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;

import brianvermeer.blue4it.springboot.servicedemo.domain.Beer;
import brianvermeer.blue4it.springboot.servicedemo.externalService.ReviewResponse;
import brianvermeer.blue4it.springboot.servicedemo.repository.BeerRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT, value = {
    "spring.profiles.active=test",
    "service.review.url=http://localhost:8889/review",
    "management.port=0"
})
public class BeerControllerTest {

    private static final String url = "/beers";

    @ClassRule
    public static WireMockClassRule wireMockRule = new WireMockClassRule(wireMockConfig().port(8889));

    @Rule
    public WireMockClassRule instanceRule = wireMockRule;

    @LocalServerPort
    private int serverPort;

    @Autowired
    private BeerRepository repository;

    @Before
    public void setUp() {
        repository.deleteAll();
        RestAssured.port = serverPort;
        repository.save(new Beer("beer1", 1.1));
        repository.save(new Beer("beer2", 2.2));
    }

    @Test
    public void allBeers() {
        given()
            .contentType(ContentType.JSON)
            .get(url)
            .peek()
            .then()
            .statusCode(200)
            .body("size()", is(2))
            .body("[0].name", equalTo("beer1"))
            .body("[1].name", equalTo("beer2"))
//            .body("name", hasItems("beer2", "beer1"))
        ;
    }

    @Test
    public void addBeer() {
        given()
            .contentType(ContentType.JSON)
            .body(new Beer("TestBeer", 42))
            .post(url + "/add")
            .then()
            .statusCode(200);

        assertThat(repository.findByName("TestBeer").isPresent()).isTrue();
        assertThat(repository.findByName("TestBeer").get().getAlcohol()).isEqualTo(42);

        given()
            .contentType(ContentType.JSON)
            .body(new Beer("TestBeer", 42))
            .post(url + "/add")
            .peek()
            .then()
            .statusCode(400)
            .body("message", containsString("Already have a beer with name:"))
            ;

    }

    @Test
    public void allBeersWithReview() throws JsonProcessingException {
        //todo stub and verify wiremock

        ObjectMapper mapper = new ObjectMapper();
        ReviewResponse response = new ReviewResponse("beer1", 5);

        wireMockRule.stubFor(get(urlPathMatching("/review/beer1"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(mapper.writeValueAsString(response))));


        given()
            .contentType(ContentType.JSON)
            .get(url+"/beer1/withscore")
            .peek()
            .then()
            .statusCode(200);

        verify(1, getRequestedFor(urlEqualTo("/review/beer1")));
    }

    @Test
    public void allBeersWithReviewWrong() throws JsonProcessingException {


        wireMockRule.stubFor(get(urlPathMatching("/review/beer1"))
            .willReturn(aResponse()
                .withStatus(404)
            ));

        given()
            .contentType(ContentType.JSON)
            .get(url+"/beer1/withscore")
            .peek()
            .then()
            .statusCode(500)
            .body("message", equalTo("Something went wrong internally"));

        verify(1, getRequestedFor(urlEqualTo("/review/beer1")));
    }



}
